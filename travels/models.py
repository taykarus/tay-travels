from django.db import models
from django.contrib.auth.models import AbstractUser


class Country(models.Model):
    name = models.CharField(max_length=255)


class State(models.Model):
    name = models.CharField(max_length=255)
    state = models.ForeignKey(Country, on_delete=models.CASCADE)


class City(models.Model):
    name = models.CharField(max_length=255)
    state = models.ForeignKey(State, on_delete=models.CASCADE)


class Address(models.Model):
    address_line = models.CharField(max_length=255)
    city = models.ForeignKey(City, on_delete=models.CASCADE)
    postal_code = models.CharField(max_length=255)


class UserType(models.Model):
    description = models.CharField(max_length=255)


class UserAccount(AbstractUser):
    type = models.ForeignKey(UserType, on_delete=models.CASCADE)
    address = models.ForeignKey(Address, on_delete=models.CASCADE)


class AccommodationType(models.Model):
    description = models.CharField(max_length=255)


class Accommodation(models.Model):
    name = models.CharField(max_length=255)
    price = models.DecimalField
    address = models.ForeignKey(Address, on_delete=models.CASCADE)


class Reservation(models.Model):
    user = models.ForeignKey(UserAccount, on_delete=models.CASCADE)
    accommodation = models.ForeignKey(Accommodation, on_delete=models.CASCADE)
    start = models.DateTimeField()
    end = models.DateTimeField()
    checkin = models.DateTimeField()
    checkout = models.DateTimeField()






